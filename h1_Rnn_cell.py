import torch

batchSize = 1
seqlen = 3
inputSize = 4
hiddenSize = 2

cell = torch.nn.RNNCell(input_size=inputSize,hidden_size=hiddenSize)
dataset = torch.randn(seqlen,batchSize,inputSize)
hidden = torch.zeros(batchSize,hiddenSize)

for idx,input in enumerate(dataset):
    print("=" * 20,idx,"="*20)
    print("Input size:",input.shape)
    hidden = cell(input,hidden)
    print("outputs size",hidden.shape)
    print(hidden)